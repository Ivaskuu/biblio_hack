# APP
- Controllare se un libro è disponibile nella bibliotecca
- Prenotare libri da andar prendere nelle 24h successive
- Suggestione libri grazie ad un sistema di machine learning che impara in base a quello che hai già letto o quelli aggiunti tra i preferiti o da leggere (quindi numero di pagine/parole, difficoltà testo, genere, autore, data di uscita), alle categorie scelte come preferite, gli autori che ti piacciono, i libri che hanno letto gli altri che hanno i tuoi interessi...
- Forum per ogni libro dove puoi parlare con altri utenti sul libro, fare domande su cosa non hai capito...
- "Vetrina" virtuale con esperienza e livello per ogni utente con i libri letti (se profilo != privato)
- Consultare cronologia libri presi in prestito, ma anche info sui libri già prenotati cioè data scadenza e penale
- Permettere agli piccoli scrittori in erba di aggiungere i loro testi e ricevere feedback dagli utenti (tipo wattpad)
- Leggere ebook
    - Alla fine, che differenza c'è tra prendere in prestito un libro o un ebook? Cosa ci guadagna la bibliotecca se l'utente viene a prenderlo in prestito in biblioteca?
    - Lasciare gli autori aggiungere effetti speciali per ogni pagina, come vento se si tratta di una foresta, cricchetti, musica suspense se si tratta di un horror, gente che parla in un bar...





# UI
## Presentation
- https://www.behance.net/gallery/53642699/Sleep-Diver?tracking_source=curated_galleries_list
- https://www.behance.net/gallery/58415343/UI-Design-Hotel-booking
- https://www.behance.net/gallery/68793775/App-for-Exploring-Adventurous-Places





## UI
### Onboarding
- https://www.uplabs.com/posts/onboarding-41fe2a02-4fba-4161-b63b-1116d8117dde
- https://assets.materialup.com/uploads/38230897-e4e7-43ae-a329-cc0071a6b2d1/attachment.png (https://www.uplabs.com/posts/read-me-mobile-app-design-kit)
- https://www.uplabs.com/posts/surf-guide-mobile-swipe-distort






### Main pages



### Gimmicks
- https://www.uplabs.com/posts/fame-lab-badges

