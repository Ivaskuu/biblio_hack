import 'package:http/http.dart' as http;
import 'dart:convert';

class Apis {
  String url = 'https://bibliohack.fastersetup.it/api/search/';

  Future<Map<String, dynamic>> getBookFromISBN(String isbn) async {
    Map<String, dynamic> map;
    await http.get('$url$isbn').then((response) {
      map = json.decode(response.body);
    });
    return map;
  }
}
