import 'package:flutter/material.dart';

class Section extends StatelessWidget {
  final String text;
  final Widget widget;

  final Function() onTap;
  final String trailing;

  const Section({Key key, this.text, this.widget, this.trailing, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 16.0).copyWith(top: 0.0),
          child: trailing != null
              ? Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      text,
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    FlatButton(
                      onPressed: onTap,
                      textColor: Colors.blue,
                      child: Text(trailing),
                    ),
                  ],
                )
              : Text(
                  text,
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
        ),
        widget,
      ],
    );
  }
}
