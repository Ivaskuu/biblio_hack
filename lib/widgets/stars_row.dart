import 'package:flutter/material.dart';

class StarsRow extends StatelessWidget {
  final double stars;
  final double size;
  const StarsRow(this.stars, {Key key, this.size: 24.0}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: List.generate(
        5,
        (i) => Icon(
              Icons.star,
              color: i <= stars ? Colors.amber : Colors.black12,
              size: size,
            ),
      ),
    );
  }
}
