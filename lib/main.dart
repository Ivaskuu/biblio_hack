import 'package:biblio_hack/pages/booksList.dart';
import 'package:biblio_hack/pages/homepage/homepage.dart';
import 'package:biblio_hack/pages/newsList.dart';
import 'package:biblio_hack/api.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

final BooksList bookList = new BooksList();
final NewsList newsList = new NewsList();
final Apis api = new Apis();

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Biblio Hackathon',
      theme: ThemeData(
        brightness: Brightness.light,
        primaryColor: Colors.white,
        // accentColor: Color(0xff0322D6),
        scaffoldBackgroundColor: Colors.white,
        fontFamily: 'GoogleSans',
      ),
      home: HomePage(
        latestBooks: bookList.latestBooks,
        suggestedBooks: bookList.suggestedBooks,
        news: newsList.news,
        api: api,
      ),
    );
  }
}
