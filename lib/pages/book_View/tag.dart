import 'package:flutter/material.dart';

class Tag extends StatelessWidget {
  const Tag({
    Key key,
    @required this.content,
    this.highlight: false,
    this.deletable: false,
    this.onTap,
  }) : super(key: key);

  final String content;
  final bool highlight;
  final bool deletable;
  final Function(String) onTap;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => onTap(content),
      child: OutlineButton(
        onPressed: null,
        disabledTextColor: Colors.black,
        disabledBorderColor: highlight ? Theme.of(context).accentColor : null,
        child: deletable
            ? Row(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Icon(
                    Icons.remove_circle_outline,
                    size: 12.0,
                    color: Colors.black45,
                  ),
                  SizedBox(width: 8.0),
                  Text(content),
                ],
              )
            : Text(content),
      ),
    );
  }
}
