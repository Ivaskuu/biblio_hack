import 'package:flutter/material.dart';

class CompatibilityBar extends StatelessWidget {
  final double perc;
  const CompatibilityBar({Key key, this.perc}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(left: 4.0),
          child: RichText(
            textAlign: TextAlign.left,
            text: TextSpan(
              style: TextStyle(
                fontSize: 16.0,
                color: Colors.black,
                fontFamily: 'GoogleSans',
              ),
              text: 'Compatibilità: ',
              children: [
                TextSpan(
                  text: '${(perc * 100).round()}%',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
          ),
        ),
        SizedBox(height: 4.0),
        SizedBox.fromSize(
          size: Size.fromHeight(20.0),
          child: Container(
            child: Material(
              color: Colors.white,
              clipBehavior: Clip.antiAlias,
              shape: StadiumBorder(),
              elevation: 6.0,
              shadowColor: Colors.black45,
              child: Container(
                margin: EdgeInsets.all(3.0),
                child: LayoutBuilder(builder: (context, constraints) {
                  final width = constraints.maxWidth;

                  return Row(
                    children: <Widget>[
                      Expanded(
                        child: Material(
                          clipBehavior: Clip.antiAlias,
                          shape: StadiumBorder(),
                          shadowColor: Colors.black45,
                          child: Container(
                            decoration: BoxDecoration(
                              gradient: LinearGradient(
                                colors: [
                                  Color(0xFF1FA2FF),
                                  Color(0xFFA6FFCB),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(width: width - (width * perc)),
                    ],
                  );
                }),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
