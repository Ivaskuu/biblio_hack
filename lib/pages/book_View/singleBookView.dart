import 'package:biblio_hack/models/book.dart';
import 'package:biblio_hack/pages/book_View/compatibility_bar.dart';
import 'package:biblio_hack/pages/book_View/tags_list.dart';
import 'package:biblio_hack/pages/booksList.dart';
import 'package:biblio_hack/pages/profile/profile_page.dart';
import 'package:biblio_hack/widgets/section.dart';
import 'package:biblio_hack/widgets/stars_row.dart';
import 'package:flutter/material.dart';

class SingleBookView extends StatefulWidget {
  final Book book;
  final bool doHero;

  SingleBookView({
    this.book,
    this.doHero,
  });

  @override
  State<StatefulWidget> createState() => _SingleBookViewState();
}

class _SingleBookViewState extends State<SingleBookView> {
  int _section = 0;
  List<Widget> sections;

  @override
  initState() {
    super.initState();
    sections = [
      _buildBookSection(),
      _buildBookReviews(),
      _buildBookQuestions(),
    ];
  }

  _changeCategory(i) {
    if (_section != i) setState(() => _section = i);
  }

  _buildBookSection() {
    return ListView(
      children: <Widget>[
        AppBar(
          elevation: 0.0,
          centerTitle: true,
          leading: BackButton(color: Colors.black),
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(16.0),
              child: SizedBox(
                height: 200.0,
                child: AspectRatio(
                  aspectRatio: 4 / 6,
                  child: Material(
                    borderRadius: BorderRadius.circular(4.0),
                    clipBehavior: Clip.antiAlias,
                    elevation: 6.0,
                    shadowColor: Colors.black45,
                    child: Image.asset(widget.book.img, fit: BoxFit.cover),
                  ),
                ),
              ),
            ),
            Flexible(
              child: Container(
                padding: EdgeInsets.all(16.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      widget.book.title,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 24.0,
                      ),
                    ),
                    Text(widget.book.author),
                    StarsRow(3.9),
                    SizedBox(height: 16.0),
                    TagsList(tags: ['Thriller', 'Azione', 'Medievale']),
                  ],
                ),
              ),
            ),
          ],
        ),
        Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 16.0,
            vertical: 8.0,
          ),
          child: CompatibilityBar(perc: 0.9),
        ),
        SizedBox(height: 16.0),
        Divider(),
        SizedBox(height: 16.0),
        Section(
          text: 'Descrizione',
          widget: Padding(
            padding: EdgeInsets.all(16.0),
            child: Text(widget.book.text ??
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec dictum magna auctor sollicitudin finibus.'),
          ),
        ),
        Divider(),
        SizedBox(height: 8.0),
        Section(
          text: 'Altri libri simili',
          widget: SizedBox.fromSize(
            size: Size.fromHeight(200.0),
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: <Widget>[
                BookWidget(
                  book: Book(
                    img: 'res/books/1984.jpg',
                    title: '1984',
                    author: 'Dorian Grey',
                  ),
                ),
                BookWidget(
                  book: Book(
                    img: 'res/books/principe.jpg',
                    title: 'Il Piccolo Principe',
                    author: 'Antoine de SaintEUxdspertj y',
                  ),
                ),
                BookWidget(
                  book: Book(
                    img: 'res/books/rich.jpg',
                    title: 'Think and Grow Rich',
                    author: 'Tuo padre',
                  ),
                ),
                BookWidget(
                  book: Book(
                    img: 'res/books/uomo.jpg',
                    title: 'Se questo è un uomo',
                    author: 'Primo Levi',
                  ),
                ),
              ],
            ),
          ),
        ),
        SizedBox(height: 64.0),
      ],
    );
  }

  _buildBookReviews() {
    return ListView(
      children: <Widget>[
        AppBar(
          elevation: 0.0,
          centerTitle: false,
          title: Text('Recensioni'),
          leading: BackButton(color: Colors.black),
        ),
        Padding(
          padding: EdgeInsets.all(16.0),
          child: Material(
            elevation: 6.0,
            shadowColor: Colors.black12,
            clipBehavior: Clip.antiAlias,
            borderRadius: BorderRadius.circular(16.0),
            color: Color(0xFFF5F5F5),
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'Recensione migliore',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20.0,
                    ),
                  ),
                  SizedBox(height: 8.0),
                  Text(
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec dictum magna auctor sollicitudin finibus. Phasellus id maximus lorem, non efficitur enim. Nam sapien nunc, faucibus non dolor et, gravida sodales dui. Curabitur suscipit dui eget elit dictum scelerisque. Mauris convallis quam quam, at convallis velit tincidunt vitae. Vestibulum commodo, turpis lobortis hendrerit condimentum, arcu augue fringilla velit, aliquam finibus arcu nisi vitae leo.',
                  ),
                  SizedBox(height: 16.0),
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          SizedBox.fromSize(
                            size: Size.fromRadius(24.0),
                            child: Material(
                              shape: CircleBorder(),
                              elevation: 6.0,
                              shadowColor: Colors.black45,
                              clipBehavior: Clip.antiAlias,
                              child: Image.asset('res/mia.jpg',
                                  fit: BoxFit.contain),
                            ),
                          ),
                          SizedBox(width: 16.0),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                'Sofia Conti',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 15.0,
                                ),
                              ),
                              Text('317 libri letti'),
                            ],
                          ),
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          Icon(Icons.thumb_up, size: 24.0, color: Colors.green),
                          SizedBox(width: 8.0),
                          Text(
                            '1.142',
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 18.0),
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
        Divider(),
        SizedBox(height: 8.0),
        Section(
          text: 'Recensione lettori',
          widget: Column(
            children: <Widget>[
              Review(stars: 2.6),
              Review(stars: 4.9),
              Review(stars: 4.4),
              Review(stars: 3.2),
            ],
          ),
        ),
        SizedBox(height: 64.0),
      ],
    );
  }

  _buildBookQuestions() {
    return ListView(
      children: <Widget>[
        AppBar(
          elevation: 0.0,
          centerTitle: false,
          title: Text('Domande'),
          leading: BackButton(color: Colors.black),
        ),
        SizedBox(height: 8.0),
        Section(
          text: 'Domande dei lettori',
          widget: Column(
            children: <Widget>[
              Question(
                  question: 'Sapete perché Mattia e Sara si sono lasciati?'),
              Question(
                  question:
                      'Qualcuno mi può spiegare perché non hanno preso la macchina?'),
            ],
          ),
        ),
        SizedBox(height: 64.0),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: _section == 0
          ? RoundedButton(
              margin: EdgeInsets.all(0.0).copyWith(bottom: 64.0),
              icon: Icons.book,
              text: 'Ottienilo',
            )
          : Container(),
      body: Column(
        children: <Widget>[
          Expanded(child: sections[_section]),
          SizedBox.fromSize(
            size: Size.fromHeight(64.0),
            child: Drawer(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  IconButton(
                    onPressed: () => _changeCategory(0),
                    color: _section == 0 ? Colors.blue : Colors.black,
                    icon: Icon(Icons.book),
                  ),
                  IconButton(
                    onPressed: () => _changeCategory(1),
                    color: _section == 1 ? Colors.blue : Colors.black,
                    icon: Icon(Icons.star),
                  ),
                  IconButton(
                    onPressed: () => _changeCategory(2),
                    color: _section == 2 ? Colors.blue : Colors.black,
                    icon: Icon(Icons.forum),
                  ),
                  IconButton(
                    onPressed: () => _changeCategory(3),
                    color: _section == 3 ? Colors.blue : Colors.black,
                    icon: Icon(Icons.help),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class Review extends StatelessWidget {
  final double stars;
  final String name;
  final String description;

  const Review({Key key, this.stars, this.name, this.description})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(16.0),
      child: ListTile(
        leading: CircleAvatar(
          backgroundImage: AssetImage('res/mia.jpg'),
        ),
        title: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Expanded(child: Text('Sofia Conti')),
            Text(
              '${stars.toStringAsFixed(1)}',
              style: TextStyle(
                color: Colors.amber,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(width: 4.0),
            StarsRow(stars, size: 18.0),
          ],
        ),
        subtitle: Column(
          children: <Widget>[
            Text(
              'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec dictum magna auctor sollicitudin finibus. Phasellus id maximus lorem, non efficitur enim. Nam sapien nunc, faucibus non dolor et, gravida sodales dui. Curabitur suscipit dui eget elit dictum scelerisque. Mauris convallis quam quam, at convallis velit tincidunt vitae. Vestibulum commodo, turpis lobortis hendrerit condimentum, arcu augue fringilla velit, aliquam finibus arcu nisi vitae leo.',
            ),
            SizedBox(height: 8.0),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Icon(Icons.thumb_up, size: 20.0, color: Colors.green),
                SizedBox(width: 8.0),
                Text(
                  '127',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class Question extends StatelessWidget {
  final String question;

  const Question({Key key, this.question}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        SizedBox(height: 8.0),

        /// Domanda
        Align(
          alignment: Alignment.centerLeft,
          child: Padding(
            padding: EdgeInsets.only(
                left: 16.0, right: 16.0, bottom: 16.0, top: 8.0),
            child: Material(
              elevation: 6.0,
              shadowColor: Colors.black45,
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topRight: Radius.circular(20.0),
                bottomLeft: Radius.circular(20.0),
                bottomRight: Radius.circular(20.0),
              ),
              child: Container(
                margin: EdgeInsets.symmetric(vertical: 8.0, horizontal: 0.0)
                    .copyWith(bottom: 8.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Container(
                      child: ListTile(
                        leading: CircleAvatar(
                          backgroundImage: AssetImage('res/mia.jpg'),
                        ),
                        title: Text(
                          'Sofia Conti',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        subtitle: Text(
                          question,
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 4.0, right: 10.0),
                      child: FlatButton.icon(
                        onPressed: () {},
                        icon: Icon(Icons.reply, color: Colors.blueAccent),
                        label: Text(
                          'Rispondi',
                          style: TextStyle(
                            color: Colors.blueAccent,
                            fontWeight: FontWeight.w700,
                            fontSize: 16.0,
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),

        /// Reply
        Align(
          alignment: Alignment.centerRight,
          child: Padding(
            padding: EdgeInsets.only(left: 64.0, right: 16.0, bottom: 16.0),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Flexible(
                  child: Material(
                    elevation: 6.0,
                    shadowColor: Colors.black45,
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20.0),
                      bottomLeft: Radius.circular(20.0),
                      bottomRight: Radius.circular(20.0),
                    ),
                    child: Container(
                      margin: EdgeInsets.all(16.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              CircleAvatar(
                                radius: 10.0,
                                backgroundImage: AssetImage('res/mia.jpg'),
                              ),
                              SizedBox(width: 8.0),
                              Text(
                                'Sofia Conti',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                          SizedBox(height: 4.0),
                          Text(
                            'Perché litigavano spesso',
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),

        /// Reply
        Align(
          alignment: Alignment.centerRight,
          child: Padding(
            padding: EdgeInsets.only(left: 64.0, right: 16.0, bottom: 16.0),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Flexible(
                  child: Material(
                    elevation: 6.0,
                    shadowColor: Colors.black45,
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20.0),
                      bottomLeft: Radius.circular(20.0),
                      bottomRight: Radius.circular(20.0),
                    ),
                    child: Container(
                      margin: EdgeInsets.all(16.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              CircleAvatar(
                                radius: 10.0,
                                backgroundImage: AssetImage('res/mia.jpg'),
                              ),
                              SizedBox(width: 8.0),
                              Text(
                                'Sofia Conti',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                          SizedBox(height: 4.0),
                          Text(
                            'Forse perché lo stava tradendo',
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),

          ///), Reply
        ),

        /// Reply
        Align(
          alignment: Alignment.centerRight,
          child: Padding(
            padding: EdgeInsets.only(left: 64.0, right: 16.0, bottom: 16.0),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Flexible(
                  child: Material(
                    elevation: 6.0,
                    shadowColor: Colors.black45,
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20.0),
                      bottomLeft: Radius.circular(20.0),
                      bottomRight: Radius.circular(20.0),
                    ),
                    child: Container(
                      margin: EdgeInsets.all(16.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              CircleAvatar(
                                radius: 10.0,
                                backgroundImage: AssetImage('res/mia.jpg'),
                              ),
                              SizedBox(width: 8.0),
                              Text(
                                'Sofia Conti',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                          SizedBox(height: 4.0),
                          Text(
                            'Non ho capito quella parte neache io...',
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),

        SizedBox(height: 8.0),
        Divider(),
      ],
    );
  }
}
