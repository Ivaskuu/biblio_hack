import 'package:biblio_hack/pages/book_View/tag.dart';
import 'package:flutter/material.dart';

class TagsList extends StatelessWidget {
  final List<String> tags;
  final List<String> highlighted;
  final bool deletable;
  final Function(String) onTap;

  const TagsList({
    Key key,
    @required this.tags,
    this.deletable: false,
    this.onTap,
    this.highlighted,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Wrap(
      direction: Axis.horizontal,
      alignment: WrapAlignment.start,
      crossAxisAlignment: WrapCrossAlignment.start,
      spacing: 12.0,
      children: tags
          .map((content) => Tag(
              content: content,
              highlight: highlighted != null && highlighted.contains(content),
              deletable: deletable,
              onTap: (_) => onTap(content)))
          .toList(),
    );
  }
}
