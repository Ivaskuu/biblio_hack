import 'package:biblio_hack/models/book.dart';
import 'package:biblio_hack/models/news.dart';
import 'package:biblio_hack/pages/book_View/singleBookView.dart';
import 'package:biblio_hack/pages/homepage/cardModel.dart';
import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  final List<Book> latestBooks;
  final List<Book> suggestedBook;
  final List<News> news;
  Home({
    this.latestBooks,
    this.suggestedBook,
    this.news,
  });

  @override
  State<StatefulWidget> createState() {
    return _HomeState();
  }
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            pinned: true,
            elevation: 0.0,
            title: Text(
              'Home',
              style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: 28.0),
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              height: 220.0,
              child: ListView.builder(
                padding: EdgeInsets.all(16.0),
                scrollDirection: Axis.horizontal,
                itemCount: widget.latestBooks.length,
                itemBuilder: (context, index) {
                  return LatestBookCard(book: widget.latestBooks[index]);
                },
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: SizedBox(
              height: 16.0,
            ),
          ),
          SliverToBoxAdapter(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                SizedBox(width: 16.0),
                Text(
                  'Libri suggeriti',
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 28.0,
                      fontWeight: FontWeight.bold),
                ),
              ],
            ),
          ),
          SliverToBoxAdapter(
            child: SizedBox(
              height: 16.0,
            ),
          ),
          SliverGrid.count(
            crossAxisCount: 3,
            childAspectRatio: 4 / 6,
            children: List.generate(widget.suggestedBook.length, (index) {
              return Padding(
                padding:
                    const EdgeInsets.only(left: 8.0, bottom: 8.0, right: 8.0),
                child: Material(
                  clipBehavior: Clip.antiAlias,
                  elevation: 6.0,
                  shadowColor: Colors.black54,
                  child: InkWell(
                    onTap: () {
                      Navigator.push(
                        context,
                        new MaterialPageRoute(
                          builder: (context) {
                            return SingleBookView(
                              book: widget.suggestedBook[index],
                              doHero: true,
                            );
                          },
                        ),
                      );
                    },
                    child: Hero(
                      tag: '${widget.suggestedBook[index].title}',
                      child: Image.asset(
                        widget.suggestedBook[index].img,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
              );
            }),
          ),
          SliverToBoxAdapter(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                SizedBox(width: 16.0),
                Text(
                  'News',
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 28.0,
                      fontWeight: FontWeight.bold),
                ),
              ],
            ),
          ),
          SliverList(
            delegate: SliverChildBuilderDelegate(
              (context, i) {
                return Material(
                  clipBehavior: Clip.antiAlias,
                  borderRadius: BorderRadius.circular(16.0),
                  child: InkWell(
                    onTap: () {},
                    child: Column(
                      children: <Widget>[
                        Image.asset(widget.news[i].img, fit: BoxFit.cover),
                        Text(widget.news[i].newsText.substring(0, 40) + '...'),
                      ],
                    ),
                  ),
                );
              },
              childCount: widget.news.length,
            ),
          ),
        ],
      ),
    );
  }
}
