import 'package:biblio_hack/models/book.dart';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:biblio_hack/pages/book_View/singleBookView.dart';

import 'package:flutter/material.dart';

class SuggestedBook extends StatelessWidget {
  final Book book;

  SuggestedBook({this.book});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
          context,
          new MaterialPageRoute(
            builder: (context) {
              return SingleBookView(book: book, doHero: true);
            },
          ),
        );
      },
      child: Material(
        elevation: 6.0,
        shadowColor: Colors.black54,
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 20.0, vertical: 30.0),
          child: Column(
            children: <Widget>[
              ClipRRect(
                borderRadius: BorderRadius.circular(20.0),
                child: Image.asset(
                  book.img,
                  filterQuality: FilterQuality.low,
                  height: 80.0,
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    book.title,
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 13.0),
                  ),
                  AutoSizeText(
                    book.description,
                    style: TextStyle(color: Colors.grey),
                    minFontSize: 10.0,
                    maxFontSize: 10.0,
                    maxLines: 2,
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
