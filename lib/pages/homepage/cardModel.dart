import 'package:biblio_hack/models/book.dart';
import 'package:biblio_hack/pages/book_View/singleBookView.dart';
import 'package:flutter/material.dart';

class LatestBookCard extends StatefulWidget {
  final Book book;
  LatestBookCard({
    this.book,
  });
  @override
  State<StatefulWidget> createState() {
    return _LatestBookCardState();
  }
}

class _LatestBookCardState extends State<LatestBookCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 800.0,
      width: 320.0,
      padding: EdgeInsets.symmetric(horizontal: 10.0),
      margin: EdgeInsets.only(right: 15.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15.0),
        color: Colors.purple[200],
        gradient: LinearGradient(
          colors: [
            Color(0xFFFEAC5E),
            Color(0xFFC779D0),
          ],
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Expanded(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Tooltip(
                  message: 'Già letto?',
                  child: IconButton(
                    splashColor: Colors.transparent,
                    icon: Icon(
                      widget.book.readed == 100 ? Icons.done : Icons.add,
                      color: widget.book.readed == 100
                          ? Colors.indigo
                          : Colors.white,
                    ),
                    onPressed: () {
                      setState(() {
                        widget.book.readed == 100
                            ? widget.book.readed = 0
                            : widget.book.readed = 100;
                      });
                    },
                  ),
                )
              ],
            ),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                '${widget.book.title}',
                style: TextStyle(
                    fontWeight: FontWeight.w500,
                    color: Colors.white,
                    fontSize: 20.0),
              ),
              Text(
                '${widget.book.description.substring(0, 50)}...',
                style: TextStyle(fontSize: 13.0, color: Colors.white),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              FlatButton(
                child: Text(
                  'Preview',
                  style: TextStyle(color: Colors.white),
                ),
                color: Colors.transparent,
                onPressed: () {
                  Navigator.push(context,
                      new MaterialPageRoute(builder: (context) {
                    return SingleBookView(
                      book: widget.book,
                      doHero: false,
                    );
                  }));
                },
              ),
            ],
          ),
        ],
      ),
    );
  }
}
