import 'package:biblio_hack/api.dart';
import 'package:biblio_hack/models/book.dart';
import 'package:biblio_hack/models/news.dart';
import 'package:biblio_hack/pages/homepage/home.dart';
import 'package:biblio_hack/pages/my_library/my_library_page.dart';
import 'package:biblio_hack/pages/profile/profile_page.dart';
import 'package:biblio_hack/pages/searchFromQR/searchFromQR.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  final List<Book> latestBooks;
  final List<Book> suggestedBooks;
  final List<News> news;
  final Apis api;
  HomePage({
    this.latestBooks,
    this.suggestedBooks,
    this.news,
    this.api,
  });
  @override
  State<StatefulWidget> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  bool isSearchActive;
  Map<int, Widget> pages;
  int _currentPage = 0;

  @override
  void initState() {
    isSearchActive = false;
    pages = {
      0: Home(
        latestBooks: widget.latestBooks,
        suggestedBook: widget.suggestedBooks,
        news: widget.news,
      ),
      1: ProfilePage(
        isMine: false,
      ),
      2: MyLibraryPage(
        books: widget.suggestedBooks,
      ),
      3: SearchFromQR(),
    };
    super.initState();
  }

  void showT() async {
    debugPrint('${await widget.api.getBookFromISBN('9788828614364')}');
  }

  @override
  Widget build(BuildContext context) {
    showT();
    return Scaffold(
      body: pages[_currentPage],
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentPage,
        onTap: (int index) {
          setState(() {
            _currentPage = index;
          });
        },
        fixedColor: Colors.white,
        type: BottomNavigationBarType.shifting,
        items: [
          BottomNavigationBarItem(
            icon: Icon(
              Icons.home,
              color: Colors.black,
            ),
            title: Text(
              'Home',
              style: TextStyle(color: Colors.black),
            ),
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.account_circle,
              color: Colors.black,
            ),
            title: Text(
              'Profilo',
              style: TextStyle(color: Colors.black),
            ),
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.book,
              color: Colors.black,
            ),
            title: Text(
              'Libreria',
              style: TextStyle(color: Colors.black),
            ),
          ),
          BottomNavigationBarItem(
            title: Text('Cerca', style: TextStyle(color: Colors.black)),
            icon: Icon(
              Icons.search,
              color: Colors.black,
            ),
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.settings,
              color: Colors.black,
            ),
            title: Text(
              'Impostazioni',
              style: TextStyle(color: Colors.black),
            ),
          ),
        ],
      ),
    );
  }
}
