import 'package:biblio_hack/models/book.dart';
import 'package:biblio_hack/pages/my_library/my_library_page.dart';
import 'package:biblio_hack/widgets/section.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class ProfilePage extends StatefulWidget {
  final bool isMine;
  ProfilePage({
    this.isMine,
  });
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  bool _followed = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      /* floatingActionButton: RoundedButton(
        text: 'Contatta',
        icon: Icons.message,
        margin: EdgeInsets.all(0.0),
        onTap: () async {
          const String url = 'mailto:account@test.com';
          if (await canLaunch(url)) {
            await launch(url);
          } else {
            throw 'Impossibile aprire la mail';
          }
        },
      ), */
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            elevation: 0.0,
            snap: true,
            floating: true,
            leading: widget.isMine
                ? IconButton(
                    tooltip: 'Modifica il profilo',
                    onPressed: () => Navigator.pop(context),
                    icon: Icon(Icons.arrow_back),
                  )
                : Container(),
            actions: <Widget>[
              !widget.isMine
                  ? IconButton(
                      onPressed: () => Navigator.pop(context),
                      icon: Icon(Icons.edit),
                    )
                  : Container(),
            ],
          ),
          SliverList(
            delegate: SliverChildListDelegate([
              SizedBox.fromSize(
                size: Size.fromRadius(64.0),
                child: Material(
                  shape: CircleBorder(),
                  elevation: 6.0,
                  shadowColor: Colors.black45,
                  clipBehavior: Clip.antiAlias,
                  child: Image.asset('res/mia.jpg', fit: BoxFit.cover),
                ),
              ),
              SizedBox(height: 16.0),
              Align(
                alignment: Alignment.center,
                child: widget.isMine
                    ? !_followed
                        ? RoundedButton(
                            onTap: () => setState(() => _followed = true),
                            innerMargin: EdgeInsets.symmetric(
                                    horizontal: 32.0, vertical: 12.0)
                                .copyWith(left: 24.0),
                            text: 'SEGUI',
                            icon: Icons.add,
                          )
                        : RoundedButton(
                            onTap: () => setState(() => _followed = false),
                            innerMargin: EdgeInsets.symmetric(
                                    horizontal: 32.0, vertical: 12.0)
                                .copyWith(left: 24.0),
                            text: 'SEGUITO',
                            icon: Icons.done,
                            decoration: BoxDecoration(color: Colors.green),
                          )
                    : Container(),
              ),
              SizedBox(height: 16.0),
              Center(child: Text('Livello 17 (389xp)')),
              Padding(
                padding: EdgeInsets.all(24.0).copyWith(bottom: 8.0, top: 8.0),
                child: Text(
                  'Sofia Conti',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 32.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 24.0)
                    .copyWith(bottom: 16.0),
                child: Text(
                  'Generi preferiti: Fantascienza, Giallo',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontWeight: FontWeight.w600),
                ),
              ),
              Divider(),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Text('Libri letti'),
                      Text(
                        '117',
                        style: TextStyle(
                          fontSize: 24.0,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      Text('Ore di lettura'),
                      Text(
                        '3h 40min',
                        style: TextStyle(
                          fontSize: 24.0,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      Text('Follower'),
                      Text(
                        '600',
                        style: TextStyle(
                          fontSize: 24.0,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              Divider(),
              Section(
                text: 'Libri preferiti (4)',
                trailing: 'Mostra tutti',
                onTap: () {},
                widget: SizedBox.fromSize(
                  size: Size.fromHeight(200.0),
                  child: ListView(
                    scrollDirection: Axis.horizontal,
                    children: <Widget>[
                      BookWidget(
                        book: Book(
                          img: 'res/books/1984.jpg',
                          title: '1984',
                          author: 'Dorian Grey',
                        ),
                      ),
                      BookWidget(
                        book: Book(
                          img: 'res/books/principe.jpg',
                          title: 'Il Piccolo Principe',
                          author: 'Antoine de SaintEUxdspertj y',
                        ),
                      ),
                      BookWidget(
                        book: Book(
                          img: 'res/books/rich.jpg',
                          title: 'Think and Grow Rich',
                          author: 'Tuo padre',
                        ),
                      ),
                      BookWidget(
                        book: Book(
                          img: 'res/books/uomo.jpg',
                          title: 'Se questo è un uomo',
                          author: 'Primo Levi',
                        ),
                      ),
                      SeeMoreBookWidget(onTap: () {}),
                    ],
                  ),
                ),
              ),
              Divider(),
              Section(
                text: 'Libri letti (117)',
                trailing: 'Mostra tutti',
                onTap: () {},
                widget: SizedBox.fromSize(
                  size: Size.fromHeight(200.0),
                  child: ListView(
                    scrollDirection: Axis.horizontal,
                    children: <Widget>[
                      BookWidget(
                        book: Book(
                          img: 'res/books/1984.jpg',
                          title: '1984',
                          author: 'Dorian Grey',
                        ),
                      ),
                      BookWidget(
                        book: Book(
                          img: 'res/books/principe.jpg',
                          title: 'Il Piccolo Principe',
                          author: 'Antoine de SaintEUxdspertj y',
                        ),
                      ),
                      BookWidget(
                        book: Book(
                          img: 'res/books/rich.jpg',
                          title: 'Think and Grow Rich',
                          author: 'Tuo padre',
                        ),
                      ),
                      BookWidget(
                        book: Book(
                          img: 'res/books/uomo.jpg',
                          title: 'Se questo è un uomo',
                          author: 'Primo Levi',
                        ),
                      ),
                      SeeMoreBookWidget(),
                    ],
                  ),
                ),
              ),
            ]),
          ),
        ],
      ),
    );
  }
}

class BookWidget extends StatelessWidget {
  final Book book;
  const BookWidget({Key key, this.book}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {},
      child: Container(
        padding: EdgeInsets.all(16.0),
        child: AspectRatio(
          aspectRatio: 4 / 6,
          child: Material(
            borderRadius: BorderRadius.circular(16.0),
            clipBehavior: Clip.antiAlias,
            elevation: 6.0,
            shadowColor: Colors.black45,
            child: Image.asset(book.img, fit: BoxFit.cover),
          ),
        ),
      ),
    );
  }
}

class SeeMoreBookWidget extends StatelessWidget {
  final Function() onTap;
  const SeeMoreBookWidget({Key key, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(16.0),
      child: AspectRatio(
        aspectRatio: 4 / 6,
        child: Material(
          color: Color(0x11000000),
          borderRadius: BorderRadius.circular(16.0),
          clipBehavior: Clip.antiAlias,
          shadowColor: Colors.black45,
          child: InkWell(
            onTap: onTap,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Icon(Icons.add),
                Text('Mostra tutti'),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class RoundedButton extends StatelessWidget {
  final String text;
  final IconData icon;
  final EdgeInsets margin;
  final EdgeInsets innerMargin;
  final BoxDecoration decoration;

  final Function() onTap;

  const RoundedButton({
    Key key,
    this.text,
    this.icon,
    this.onTap,
    this.margin: const EdgeInsets.all(24.0),
    this.innerMargin:
        const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
    this.decoration: const BoxDecoration(
      gradient: LinearGradient(
        colors: [
          Color(0xFFFEAC5E),
          Color(0xFFC779D0),
        ],
        begin: Alignment.topLeft,
        end: Alignment.bottomRight,
      ),
    ),
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: margin,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          InkWell(
            onTap: onTap,
            child: Material(
              clipBehavior: Clip.antiAlias,
              shape: StadiumBorder(),
              elevation: 6.0,
              shadowColor: Colors.black45,
              child: Container(
                padding: innerMargin,
                child: Row(
                  children: <Widget>[
                    icon != null
                        ? Icon(
                            icon,
                            color: Colors.white,
                            size: 20.0,
                          )
                        : Container(),
                    icon != null ? SizedBox(width: 8.0) : Container(),
                    Text(
                      text,
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
                decoration: decoration,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
