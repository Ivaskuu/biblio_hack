import 'package:biblio_hack/models/news.dart';

class NewsList {
  List<News> news = [
    News(
        title: '1984',
        newsText:
            'E\' ora disponibile il libro 1984 di George Orwell nella tua biblioteca.',
        img: 'res/biblioClosed.jpg'),
    News(
        title: 'Biblioteca chiusa',
        newsText:
            'Biblioteca di Cuneo verrà chiusa fino al 6 di gennaio per la festa della befana',
        img: 'res/biblioClosed.jpg'),
    News(
        title: 'Altra notizia molto interessante',
        newsText:
            'Decisamente interessante, stai attento che potresti rimanerci secco',
        img: 'res/biblioClosed.jpg'),
  ];
}
