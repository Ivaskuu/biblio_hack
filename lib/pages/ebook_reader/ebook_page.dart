import 'package:flutter/material.dart';
import 'package:flutter_tts/flutter_tts.dart';

import 'dart:async' show Future;
import 'package:flutter/services.dart' show rootBundle;

class EbookPage extends StatefulWidget {
  @override
  _EbookPageState createState() => _EbookPageState();
}

class _EbookPageState extends State<EbookPage> {
  final ebookTextStyle = TextStyle(
    height: 1.5,
    fontFamily: 'Bookerly',
    fontSize: 18.0,
  );

  final lineHeight = 36; // Più o meno

  int page = 0;
  int maxPages;
  String text;

  FlutterTts flutterTts = FlutterTts();
  bool isTtsSpeaking = false;

  final String choiceText =
      '''Ad un certo tratto la cabina inizia a riempirsi di un fumo nero e denso.
L'aereo sta precipitando. Cosa deve fare Eduardo?

Opzione 1: Andare nella cabina di pilotaggio e cercare di prendere il controllo dell'aereo
Opzione 2: Buttarsi giù dall'aereo con il paracadute

Seleziona la tua scelta per continuare.''';

  final String choice1 =
      'Eduardo si avvicina alla porta della cabina, ma purtroppo è chiusa. BOOK OVER.';

  @override
  void initState() {
    super.initState();

    _load1984().then((testo) {
      setState(() {
        text = testo;
      });
      // flutterTts.speak(text);
    });

    // Future.delayed(Duration(seconds: 10), () => flutterTts.stop());
  }

  Future _load1984() async {
    return rootBundle.loadString('res/1984.txt');
  }

  _calcBookPages(constraints, linesPerPage) {
    final span = TextSpan(text: text, style: ebookTextStyle);
    final tp = TextPainter(text: span, textDirection: TextDirection.ltr);
    tp.layout(maxWidth: constraints.maxWidth);

    Future.delayed(
        Duration.zero,
        () => setState(
            () => maxPages = ((tp.height / lineHeight) / linesPerPage).ceil()));
  }

  _playPauseTts() {
    setState(() {
      if (isTtsSpeaking) {
        flutterTts.stop();
      } else {
        flutterTts.speak(choiceText);
        Future.delayed(Duration(seconds: 23), () {
          showDialog(
            context: context,
            builder: (context) {
              return SimpleDialog(
                title: Text('Cosa deve fare Eduardo?'),
                children: <Widget>[
                  FlatButton(
                    onPressed: () {
                      Navigator.pop(context);
                      setState(() {
                        page = 0;
                        text = choice1;
                        flutterTts.speak(choice1);

                        Future.delayed(Duration(seconds: 7), () {
                          showDialog(
                            context: context,
                            builder: (context) {
                              return SimpleDialog(
                                title: Text('Vuoi ritornare indietro?'),
                                children: <Widget>[
                                  FlatButton(
                                    onPressed: () => Navigator.pop(context),
                                    textColor: Colors.red,
                                    child: Text('RIPROVA'),
                                  ),
                                ],
                              );
                            },
                          );
                        });
                      });
                    },
                    textColor: Colors.blue,
                    child: Text('Prendere il controllo'),
                  ),
                  FlatButton(
                    onPressed: () {},
                    textColor: Colors.red,
                    child: Text('Buttarsi'),
                  ),
                ],
              );
            },
          );
        });
      }

      isTtsSpeaking = !isTtsSpeaking;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        brightness: Brightness.light,
        backgroundColor: Colors.white,
        elevation: 0.0,
        title: Text(
          'Le avventure di Eduardo',
          style: TextStyle(
            fontSize: 24.0,
            fontWeight: FontWeight.bold,
            color: Colors.black,
          ),
        ),
        actions: <Widget>[
          Padding(
            padding: EdgeInsets.only(right: 16.0),
            child: Center(
              child: Text(
                '${page + 173}/897',
                style: TextStyle(
                  color: Colors.black,
                ),
              ),
            ),
          ),
        ],
      ),
      backgroundColor: Colors.white, //Color(0xFFf5e5cc),
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(24.0).copyWith(top: 0.0, bottom: 48.0),
            child: LayoutBuilder(builder: (context, constraints) {
              if (text != null) {
                final maxLines = constraints.maxHeight ~/ lineHeight;
                final offset = maxLines *
                    lineHeight; // https://upload.wikimedia.org/wikipedia/commons/6/6d/Offset_Veld_festival_2017.jpg

                if (maxPages == null) _calcBookPages(constraints, maxLines);

                return Stack(
                  overflow: Overflow.clip,
                  fit: StackFit.expand,
                  children: <Widget>[
                    Positioned(
                      top: -(page * offset * 1.0),
                      width: constraints.maxWidth,
                      child: Text(
                        text,
                        maxLines: maxLines * (page + 1),
                        textAlign: TextAlign.justify,
                        style: ebookTextStyle,
                      ),
                    ),
                  ],
                );
              } else
                return Center(
                  child: CircularProgressIndicator(),
                );
            }),
          ),
          Row(
            children: <Widget>[
              Flexible(
                flex: 1,
                child: GestureDetector(
                    onTap: () => page > 0 ? setState(() => page--) : null),
              ),
              Flexible(
                flex: 1,
                child: GestureDetector(
                  onTap: () => page < (maxPages ?? 0) - 1
                      ? setState(() => page++)
                      : null,
                ),
              ),
            ],
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                IconButton(
                  onPressed: _playPauseTts,
                  icon: Icon(isTtsSpeaking ? Icons.pause : Icons.play_arrow),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

// F5E5CC
