import 'package:biblio_hack/models/book.dart';

class BooksList {
  final List<Book> latestBooks = [
    Book(
      img: 'res/books/edward.jpg',
      title: 'Le avventure di Eduardo',
      author: 'Kate di Camillo',
      description:
          'Scegli tu le prossime mosse di Eduardo. In questo nuovo e innovativo libro interattivo...',
      readed: 0,
    ),
    Book(
      img: 'res/books/1984.jpg',
      title: '1984',
      author: 'Dorian Grey',
      readed: 0,
      description:
          'Ho visto il film, nulla di che(neanche l\'ho finito) pero\' da quello che ho visto mi e\' piaciuto un pochino.',
    ),
    Book(
      img: 'res/books/principe.jpg',
      title: 'Il Piccolo Principe',
      author: 'Antoine de Saint-Exupéry',
      description:
          'Anche qui non ne ho idea, pero\' il libro mi era piaciuto, credimi sulla parola',
      readed: 0,
    ),
    Book(
      img: 'res/books/rich.jpg',
      title: 'Think and Grow Rich',
      author: 'Tuo padre',
      description:
          'Qui non so proprio cosa mettere, quindi continuero\' a scrivere cose a caso finche\' i caratteri non basteranno.',
      readed: 0,
    ),
    Book(
      img: 'res/books/uomo.jpg',
      title: 'Se questo è un uomo',
      author: 'Primo Levi',
      description:
          'Se questo e\'un uomo io direi che non e\' una bella persona, poi vedi tu a chi credere.',
      readed: 0,
    ),
  ];
  final List<Book> suggestedBooks = [
    Book(
      img: 'res/books/edward.jpg',
      title: 'Le avventure di Eduardo',
      author: 'Kate di Camillo',
      description: 'Scegli tu le prossime mosse di Eduardo.',
      readed: 0,
    ),
    Book(
        img: 'res/books/1984.jpg',
        title: '1984',
        author: 'Dorian Grey',
        readed: 100,
        description:
            'Ho visto il film, nulla di che(neanche l\'ho finito) pero\' da quello che ho visto mi e\' piaciuto un pochino.'),
    Book(
      img: 'res/books/principe.jpg',
      title: 'Il Piccolo Principe',
      author: 'Antoine de Saint-Exupéry',
      description:
          'Anche qui non ne ho idea, pero\' il libro mi era piaciuto, credimi sulla parola',
      readed: 100,
    ),
    Book(
      img: 'res/books/rich.jpg',
      title: 'Think and Grow Rich',
      author: 'Tuo padre',
      description:
          'Qui non so proprio cosa mettere, quindi continuero\' a scrivere cose a caso finche\' i caratteri non basteranno.',
      readed: 43,
    ),
    Book(
      img: 'res/books/uomo.jpg',
      title: 'Se questo è un uomo',
      author: 'Primo Levi',
      description:
          'Se questo e\'un uomo io direi che non e\' una bella persona, poi vedi tu a chi credere.',
      readed: 27,
    ),
  ];
}
