import 'package:biblio_hack/models/book.dart';
import 'package:biblio_hack/pages/book_View/singleBookView.dart';
import 'package:biblio_hack/pages/ebook_reader/ebook_page.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';

class MyLibraryPage extends StatefulWidget {
  final List<Book> books;
  MyLibraryPage({this.books});

  @override
  _MyLibraryPageState createState() => _MyLibraryPageState();
}

class _MyLibraryPageState extends State<MyLibraryPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        title: Text(
          'I miei libri',
          style: TextStyle(
            fontSize: 24.0,
            fontWeight: FontWeight.bold,
            color: Colors.black,
          ),
        ),
      ),
      body: SizedBox.expand(
        child: CarouselSlider(
          realPage: 3,
          viewportFraction: 0.8,
          aspectRatio: 9 / 16,
          items: widget.books.map((book) => BookView(book: book)).toList(),
        ),
      ),
    );
  }
}

class BookView extends StatelessWidget {
  final Book book;
  const BookView({Key key, this.book}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.push(
          context, MaterialPageRoute(builder: (_) => EbookPage())),
      child: Container(
        padding: EdgeInsets.all(32.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            AspectRatio(
              aspectRatio: 4 / 6,
              child: Material(
                borderRadius: BorderRadius.circular(16.0),
                clipBehavior: Clip.antiAlias,
                elevation: 6.0,
                shadowColor: Colors.black45,
                child: Hero(
                    child: Image.asset(book.img, fit: BoxFit.cover),
                    tag: '${book.title}'),
              ),
            ),
            Column(
              children: <Widget>[
                Text(
                  book.title,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 24.0,
                    fontWeight: FontWeight.w600,
                    color: Colors.black,
                  ),
                ),
                Text('di ${book.author}', textAlign: TextAlign.center),
                OutlineButton(
                  child: Text(
                    'Maggiori informazioni',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  onPressed: () {
                    Navigator.push(
                      context,
                      new MaterialPageRoute(
                        builder: (context) {
                          return SingleBookView(
                            book: book,
                            doHero: true,
                          );
                        },
                      ),
                    );
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
