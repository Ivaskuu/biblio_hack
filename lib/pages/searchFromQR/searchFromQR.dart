import 'package:biblio_hack/models/book.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';

String barcodeScanRes;

class SearchFromQR extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _SearchFromQRState();
  }
}

class _SearchFromQRState extends State<SearchFromQR> {
  Book book = new Book();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Scanner QR'),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            FlatButton(
              child: Text('Scannerizza il codice'),
              onPressed: () async {
                barcodeScanRes = await FlutterBarcodeScanner.scanBarcode('000');
              },
            ),
            Expanded(child: Text(book.title ?? 'Nessun libro')),
          ],
        ),
      ),
    );
  }
}

class BarcodeScanner {
  static const CameraAccessDenied = 'PERMISSION_NOT_GRANTED';

  static const MethodChannel _channel =
      const MethodChannel('com.apptreesoftware.barcode_scan');

  static Future<String> scan() async => await _channel.invokeMethod('scan');
}
