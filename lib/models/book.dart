class Book {
  final String isbn;
  final String title;
  final String author;
  final String description;

  int readed;

  final String text;

  final String img;
  final int numPages;
  final Map<String, int> chapters;

  Book({
    this.isbn,
    this.title,
    this.author,
    this.text,
    this.img,
    this.numPages,
    this.chapters,
    this.description,
    this.readed,
  });
}
