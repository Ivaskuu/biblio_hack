class News {
  final String title;
  final String newsText;
  final String img;

  News({
    this.title,
    this.newsText,
    this.img,
  });
}
